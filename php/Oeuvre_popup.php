<?php
include("connexion_bdd.php");

$id=htmlentities($_GET['id']);

$rq_sql ="SELECT COUNT(DISTINCT EXEMPLAIRE.noExemplaire) as nbExemplaire
            , COUNT(*) as nbEmprunts
            FROM OEUVRE
            LEFT JOIN EXEMPLAIRE
            ON OEUVRE.noOeuvre = EXEMPLAIRE.noOeuvre
            LEFT JOIN EMPRUNT
            ON EXEMPLAIRE.noExemplaire = EMPRUNT.noExemplaire
            WHERE OEUVRE.noOeuvre=".$id.";
        ";
$rp = $bdd->query($rq_sql);
$data = $rp->fetchAll();

foreach ($data as $key) {
    if ($key['nbEmprunts'] == 0 and $key['nbExemplaire'] == 0) {
        $ma_requete_SQL="DELETE FROM OEUVRE WHERE noOeuvre = ".$id.";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Oeuvre_show.php");
    }
}
?>

<script>
    var r = confirm("<?php
        foreach ($data as $key) {
            echo "Attention ! Supprimer cet oeuvre supprimera ".$key['nbExemplaire']." exemplaire(s) et ".$key['nbEmprunts']." emprunt(s).";
        }
        ?>");
    if (r) {
        r = false;
        document.location.href = "Oeuvre_delete.php?delete=true&<?php echo $_GET['id'] ?>";
    }
    else {
        document.location.href = "Oeuvre_show.php";
    }
</script>