<?php
include("connexion_bdd.php");
include("v_head.php");
include("v_nav.php");

if(isset($_POST["nomAuteur"]) AND isset($_POST["prenomAuteur"]))
{
    $erreurs=array();
    $donnees['nomAuteur']=htmlentities($_POST['nomAuteur']);
    $donnees['prenomAuteur']=htmlentities($_POST['prenomAuteur']);

    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['nomAuteur'])) {
        $erreurs['nomAuteur'] = "Le nom de l'auteur doit être composé d'au minimum deux lettres et/ou chiffres";
    }

    if (empty($erreurs)) {
        $ma_requete_SQL = 'INSERT INTO AUTEUR (idAuteur,nomAuteur,prenomAuteur) VALUES (NULL,"'.$donnees['nomAuteur'].'","'.$donnees['prenomAuteur'].'");';
        $bdd->exec($ma_requete_SQL);
        header("Location: Auteur_show.php");
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }

}

?>

<form method="post" action="Auteur_add.php">
    <div class="row">
        <fieldset>
            <legend>Ajouter un auteur</legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <label>Nom
                <?php
                    if (isset($donnees['nomAuteur'])) {
                        echo '<input name="nomAuteur" type="text" size="18" value="'.$donnees['nomAuteur'].'"/>';
                    }
                    else
                        echo '<input name="nomAuteur" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['nomAuteur'])) echo '<div class="alert alter-danger">'.$erreurs['nomAuteur'].'</div>';?>
            <br>
            <label>Prénom
                <input name="prenomAuteur" type="text" size="18" value="<?php
                    if (isset($donnees['prenomAuteur'])) {
                        echo $donnees['prenomAuteur'];
                    }
                ?>"/>
            </label>
            <br>
            <input type="submit" name="addAuteur" value="Ajouter un auteur"/>
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>