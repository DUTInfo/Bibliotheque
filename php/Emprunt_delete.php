<?php

include("connexion_bdd.php");

if(isset($_GET['idAdherent']) AND isset($_GET['noExemplaire']) AND isset($_GET['dateEmprunt']))
{
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_GET['idAdherent']);
    $donnees['noExemplaire']=htmlentities($_GET['noExemplaire']);
    $donnees['dateEmprunt']=htmlentities($_GET['dateEmprunt']);

    if (preg_match("/[1-9]{1,}/", $donnees['idAdherent']) AND preg_match("/[1-9]{1,}/", $donnees['noExemplaire'])
        AND preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateEmprunt'], $matches)
        AND checkdate($matches[2], $matches[1], $matches[3])) {
        $donnees['dateEmprunt_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        $ma_requete_SQL="DELETE FROM EMPRUNT WHERE idAdherent = '".$donnees['idAdherent']."' AND noExemplaire = '".$donnees['noExemplaire']."' AND dateEmprunt = '".$donnees['dateEmprunt_us']."';";
        $bdd->exec($ma_requete_SQL);
        header("Location: Emprunt_show.php");
    }
    else {
        header("Location: Emprunt_show.php");
    }
} ?>