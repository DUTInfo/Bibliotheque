<?php
function isNotValidDateFR($maDate) {
    if (preg_match("/^([0-9]{1,2})[-\/]([0-9]{1,2})[-\/]([0-9]{4})$/", $maDate, $matches)) {
        if (checkdate($matches[2], $matches[1], $matches[3])) {
            return false;
        }
        else {
            return "Date non correcte, qui n'existe pas : ".$matches[1]."-".$matches[2]."-".$matches[3];
        }
    }
    else {
        return "Format de date non correct : JJ/MM/AAAA";
    }
}

function convert_date_fr_us($maDate) {
    if (preg_match("/^([0-9]{1,2})[-\/]([0-9]{1,2})[-\/]([0-9]{4})$/", $maDate, $matches)) {
        return $matches[3]."-".$matches[2]."-".$matches[1];
    } else
        return false;
}

function convert_date_us_fr($maDate) {
    if (preg_match("/^([0-9]{4})[-\/]([0-9]{1,2})[-\/]([0-9]{1,2})$/", $maDate, $matches)) {
        return $matches[3]."/".$matches[2]."/".$matches[1];
    }
    else
        return false;
}