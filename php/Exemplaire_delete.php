<?php
include("connexion_bdd.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET["noExemplaire"]) AND is_numeric($_GET["noExemplaire"]) AND isset($_GET["idOeuvre"]))
{
    $id=htmlentities($_GET['noExemplaire']);
    $ma_requete_SQL="DELETE FROM EXEMPLAIRE WHERE noExemplaire = ".$id.";";
    $bdd->exec($ma_requete_SQL);
    header("Location: Exemplaire_show.php?idOeuvre=".$_GET['idOeuvre']);
}
else
    header("Location: Oeuvre_show.php");