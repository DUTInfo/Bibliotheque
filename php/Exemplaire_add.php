<?php
include("connexion_bdd.php");
include("v_head.php");
include("v_nav.php");

if (! isset($_GET['idOeuvre'])) {
    header("Location: Oeuvre_show.php");
}

if(isset($_GET['idOeuvre']) AND is_numeric($_GET['idOeuvre']) AND isset($_POST["dateAchat"]) AND isset($_POST['prix']))
{
    $erreurs=array();
    $id=htmlentities($_GET['idOeuvre']);
    $donnees['dateAchat']=htmlentities($_POST['dateAchat']);
    $donnees['prix']=htmlentities($_POST['prix']);

    if (!is_numeric($donnees['prix'])) {
        $erreurs['prix']="Le prix doit être un nombre";
    }
    else if ($donnees['prix'] == "") {
        $erreurs['prix']="Le prix ne peut pas être vide";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateAchat'], $matches)) {
        $erreurs['dateAchat']='La date d\'achat doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['dateAchat']="La date n'est pas valide";
        } else {
            $donnees['dateAchat_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    if (!isset($_POST['etat'])) {
        $erreurs['etat']="Choisissez un état";
        $message = "Il y a des erreurs dans le formulaire :";
    }
    else {
        $donnees['etat']=htmlentities($_POST['etat']);
        if (empty($erreurs)) {
            $ma_requete_SQL = 'INSERT INTO EXEMPLAIRE (noExemplaire,etat,dateAchat, prix, noOeuvre) VALUES (NULL,"'.$donnees['etat'].'","'.$donnees['dateAchat_us'].'",'.$donnees['prix'].','.$id.');';
            $bdd->exec($ma_requete_SQL);
            header("Location: Exemplaire_show.php?idOeuvre=".$id."");
        }
        else {
            $message = "Il y a des erreurs dans le formulaire :";
        }
    }
}

$ma_requete_SQL="
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, OEUVRE.noOeuvre
FROM AUTEUR
INNER JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
WHERE OEUVRE.noOeuvre = ".$_GET['idOeuvre'].";
";
$reponse = $bdd->query($ma_requete_SQL);
$donnees2 = $reponse->fetchAll();

?>

<form method="post" action="Exemplaire_add.php?idOeuvre=<?php echo $_GET['idOeuvre'] ?>">
    <div class="row">
        <fieldset><?php foreach ($donnees2 as $value) ?>
            <legend>Ajouter un exemplaire de "<?php echo $value['titre'] ?>", de <?php echo $value['nomAuteur'] ?></legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <br>
            <label>État
                <select name="etat">
                    <option value="" <?php if (!isset($_POST['etat'])) echo 'selected="selected"'; ?> selected disabled>Choisir un état</option>
                    <option value="neuf" <?php if (isset($_POST['etat']) and $_POST['etat']=="neuf") echo 'selected="selected"'; ?>>neuf</option>
                    <option value="bon" <?php if (isset($_POST['etat']) and $_POST['etat']=="bon") echo 'selected="selected"'; ?>>bon</option>
                    <option value="moyen" <?php if (isset($_POST['etat']) and $_POST['etat']=="moyen") echo 'selected="selected"'; ?>>moyen</option>
                    <option value="mauvais" <?php if (isset($_POST['etat']) and $_POST['etat']=="mauvais") echo 'selected="selected"'; ?>>mauvais</option>
                </select>
            </label>
            <br>
            <br>
            <?php if (isset($erreurs['etat'])) echo '<div class="alert alter-danger">'.$erreurs['etat'].'</div>';?>
            <label>Date d'achat
                <?php
                if (isset($_POST['dateAchat'])) {
                    echo '<input name="dateAchat" type="text" size="18" value="'.$_POST['dateAchat'].'"/>';
                }
                else
                    echo '<input name="dateAchat" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['dateAchat'])) echo '<div class="alert alter-danger">'.$erreurs['dateAchat'].'</div>';?>
            <br>
            <label>Prix
                <input name="prix" type="text" size="18" value="<?php if(isset($donnees['prix'])) echo $donnees['prix']; ?>"/>
            </label>
            <?php if (isset($erreurs['prix'])) echo '<div class="alert alter-danger">'.$erreurs['prix'].'</div>';?>
            <input type="submit" name="addExemplaire" value="Ajouter un exemplaire"/>
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>