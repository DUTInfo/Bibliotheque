<?php
include("connexion_bdd.php");

if(isset($_GET["id"]) AND is_numeric($_GET["id"]))
{
    $id=htmlentities($_GET['id']);

    if (isset($_GET['delete'])) {
        if ($_GET['delete'] == "true") {
            $ma_requete_SQL="DELETE FROM OEUVRE WHERE noOeuvre = ".$id.";";
            $bdd->exec($ma_requete_SQL);
            header("Location: Oeuvre_show.php");
        }
        else
            header("Location: Oeuvre_show.php");
    }
}
else
    header("Location: Oeuvre_show.php");