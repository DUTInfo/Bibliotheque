<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

// ## accès au modèle
$ma_requete_SQL = "
SELECT ADHERENT.idAdherent, EXEMPLAIRE.noExemplaire, OEUVRE.titre, ADHERENT.nomAdherent, EMPRUNT.dateEmprunt, EMPRUNT.dateRendu
, DATEDIFF(curdate(), EMPRUNT.dateEmprunt) AS nbJoursEmprunt
, DATEDIFF(curdate(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY)) AS retard
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY), 1, 0) AS flagRetard
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY), 1, 0) AS flagPenalite
, IF(((DATEDIFF(CURRENT_DATE(),DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY)) * 0.5) < 25), 
    (DATEDIFF(CURRENT_DATE(),DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY)) * 0.5), 25) AS dette
FROM ADHERENT
INNER JOIN EMPRUNT
ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE
ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE
ON EXEMPLAIRE. noOeuvre = OEUVRE.noOeuvre
ORDER BY EMPRUNT.dateEmprunt DESC;
";
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();
?>

<div class="row">
    <a href="Emprunt_add.php">Ajouter un emprunt</a>
	<table border="2">
		<caption>Récapitulatifs des emprunts</caption>
        <?php if(isset($donnees[0])): ?>
			<thead>
				<tr>
                    <th>Nom de l'adhérent</th>
                    <th>Titre de l'oeuvre empruntée</th>
                    <th>Date d'emprunt</th>
                    <th>Date de restitution</th>
                    <th>Exemplaire</th>
                    <th>Retard</th>
                    <th>Opérations</th>
                </tr>
			</thead>
			<tbody>
				<?php foreach ($donnees as $value): ?>
				<tr>
					<td>
						<?php echo $value['nomAdherent']; ?>
					</td>
					<td>
						<?php echo($value['titre']); ?>
					</td>
					<td>
						<?php echo convert_date_us_fr($value['dateEmprunt']); ?>
					</td>
                    <td>
                        <?php echo convert_date_us_fr($value['dateRendu']); ?>
                    </td>
                    <td>
                        <?php echo $value['noExemplaire']; ?>
                    </td>
                    <td>
                        <?php
                        if ($value['retard'] > 0) {
                            echo $value['retard'];
                        }
                        ?>
                    </td>
                    <td>
                        <a class="lienTab" href="Emprunt_edit.php?idAdherent=<?= $value['idAdherent']; ?>&noExemplaire=<?= $value['noExemplaire']; ?>&dateEmprunt=<?= convert_date_us_fr($value['dateEmprunt']); ?>">Modifier</a>
                        <a class="lienTab" href="#" onclick='if(confirm("Êtes-vous sûr de vouloir supprimer cet emprunt en cours ?")) location.href="Emprunt_delete.php?idAdherent=<?= $value['idAdherent']; ?>&noExemplaire=<?= $value['noExemplaire']; ?>&dateEmprunt=<?= convert_date_us_fr($value['dateEmprunt']); ?>";'>Supprimer</a>
                    </td>
				</tr>
				<?php endforeach; ?>
			</tbody>
        <?php else: ?>
            <tr>
                <td>Pas d'emprunt dans la base de données.</td>
            </tr>
        <?php endif; ?>
	</table>
<div>

<?php include("v_foot.php"); ?>