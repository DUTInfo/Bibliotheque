<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET['idAdherent']) AND isset($_GET['noExemplaire']) AND isset($_GET['dateEmprunt']))
{
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_GET['idAdherent']);
    $donnees['noExemplaire']=htmlentities($_GET['noExemplaire']);
    $donnees['dateEmprunt']=htmlentities($_GET['dateEmprunt']);

    if (preg_match("/[1-9]{1,}/", $donnees['idAdherent']) AND preg_match("/[1-9]{1,}/", $donnees['noExemplaire'])
        AND preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateEmprunt'], $matches)
        AND checkdate($matches[2], $matches[1], $matches[3])) {
        $donnees['dateEmprunt_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        $ma_requete_SQL="
          SELECT em.idAdherent, em.noExemplaire, em.dateEmprunt, em.dateRendu FROM EMPRUNT em WHERE idAdherent = '".$donnees['idAdherent']."'
          AND noExemplaire = '".$donnees['noExemplaire']."' AND dateEmprunt = '".$donnees['dateEmprunt_us']."';";
        $reponse = $bdd->query($ma_requete_SQL);
        $donnees = $reponse->fetch();
    }
}

if(isset($_POST['newIdAdherent']) AND isset($_POST['newNoExemplaire']) AND isset($_POST['newDateEmprunt']))
{
    $erreurs=array();
    $donnees['newIdAdherent']=htmlentities($_POST['newIdAdherent']);
    $donnees['newNoExemplaire']=htmlentities($_POST['newNoExemplaire']);
    $donnees['newDateEmprunt']=htmlentities($_POST['newDateEmprunt']);
    if (isset($_POST['newDateRendu']) AND !$_POST['newDateRendu'] == "") {
        $donnees['newDateRendu']=htmlentities($_POST['newDateRendu']);

        if (!$donnees['newDateRendu'] != "" OR !preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['newDateRendu'], $matches))
            $erreurs['dateRendu']='La date de restitution doit être au format JJ/MM/AAAA';

        else if (!$donnees['newDateRendu'] != "" OR !checkdate($matches[2], $matches[1], $matches[3])) $erreurs['newDateRendu']="La date n'est pas valide";

        else $donnees['newDateRendu_us']=$matches[3]."-".$matches[2]."-".$matches[1];
    }

    if (!preg_match("/[1-9]{1,}/", $donnees['newIdAdherent'])) $erreurs['idAdherent'] = "L\'ID de l'adhérent est invalide";

    if (!preg_match("/[1-9]{1,}/", $donnees['newNoExemplaire'])) $erreurs['noExemplaire'] = "L'exemplaire selectionné ne peut pas être emprunté";

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['newDateEmprunt'], $matches))
        $erreurs['dateEmprunt']='La date d\'emprunt doit être au format JJ/MM/AAAA';

    else if (!checkdate($matches[2], $matches[1], $matches[3])) $erreurs['dateEmprunt']="La date entrée n'existe pas";

    else $donnees['newDateEmprunt_us']=$matches[3]."-".$matches[2]."-".$matches[1];

    if (empty($erreurs)) {
        if (isset($donnees['newDateRendu_us'])) {
            $ma_requete_SQL="UPDATE EMPRUNT SET idAdherent='".$donnees['newIdAdherent']."'
            , noExemplaire='".$donnees['newNoExemplaire']."', dateEmprunt='".$donnees['newDateEmprunt_us']."'
            , dateRendu = '".$donnees['newDateRendu_us']."' WHERE idAdherent = '".$donnees['idAdherent']."'
            AND noExemplaire = '".$donnees['noExemplaire']."' AND dateEmprunt = '".$donnees['dateEmprunt']."';";
        } else {
            $ma_requete_SQL="UPDATE EMPRUNT SET idAdherent='".$donnees['newIdAdherent']."'
            , noExemplaire='".$donnees['newNoExemplaire']."', dateEmprunt='".$donnees['newDateEmprunt_us']."'
            , dateRendu = NULL WHERE idAdherent = '".$donnees['idAdherent']."'
            AND noExemplaire = '".$donnees['noExemplaire']."' AND dateEmprunt = '".$donnees['dateEmprunt']."';";
        }
        $bdd->exec($ma_requete_SQL);
        header("Location: Emprunt_show.php");
    }
    else $message = "Il y a des erreurs dans le formulaire :";
}

$ma_requete_SQL="SELECT idAdherent, nomAdherent FROM ADHERENT ORDER BY nomAdherent;";
$reponse = $bdd->query($ma_requete_SQL);
$donneesAdherent = $reponse->fetchAll();
$ma_requete_SQL="
SELECT EXEMPLAIRE.noExemplaire
, EXEMPLAIRE.noExemplaire
, EXEMPLAIRE.etat
, OEUVRE.titre
, OEUVRE.noOeuvre
FROM EXEMPLAIRE
INNER JOIN OEUVRE
ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
INNER JOIN EMPRUNT
ON EXEMPLAIRE.noExemplaire = EMPRUNT.noExemplaire
LEFT JOIN EXEMPLAIRE E2
ON E2.noExemplaire = EXEMPLAIRE.noExemplaire
AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
WHERE E2.noExemplaire IS NOT NULL OR (EMPRUNT.idAdherent = '".$donnees['idAdherent']."' AND EMPRUNT.noExemplaire = '".$donnees['noExemplaire']."'
        AND EMPRUNT.dateEmprunt = '".$donnees['dateEmprunt']."')
ORDER BY OEUVRE.titre ASC, E2.noExemplaire ASC";
$reponse = $bdd->query($ma_requete_SQL);
$donneesExemplaire = $reponse->fetchAll();
$today = getdate(); ?>

    <form method="post" action="Emprunt_edit.php?idAdherent=<?= $donnees['idAdherent']; ?>&noExemplaire=<?= $donnees['noExemplaire']; ?>&dateEmprunt=<?= convert_date_us_fr($donnees['dateEmprunt']); ?>">
        <div class="row">
            <fieldset>
                <legend>Éditer une Emprunt</legend>
                <?php if (!empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
                <label>Adherent :
                    <select name="newIdAdherent">
                        <?php if(!isset($donnees['idAdherent'])): ?>
                            <option value="" selected disabled>Choisir l'adherent</option>
                        <?php endif; ?>
                        <?php foreach ($donneesAdherent as $adherent) : ?>
                            <option value="<?php echo $adherent['idAdherent']; ?>"
                                <?php if(isset($donnees['idAdherent']) and $donnees['idAdherent'] == $adherent['idAdherent']) echo "selected"; ?>
                            ><?php echo $adherent['nomAdherent']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                </label>
                <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                <br>
                <br>
                <label>Numéro d'exemplaire :
                    <select name="newNoExemplaire">
                        <?php if(!isset($donnees['noExemplaire'])): ?>
                            <option value="" selected disabled>Choisir le n° d'oeuvre</option>
                        <?php endif; ?>
                        <?php foreach ($donneesExemplaire as $exemplaire) : ?>
                            <option value="<?php echo $exemplaire['noExemplaire']; ?>"
                                <?php if(isset($donnees['noExemplaire']) and $donnees['noExemplaire'] == $exemplaire['noExemplaire']) echo "selected"; ?>
                            ><?php echo $exemplaire['titre']." -- ".$exemplaire['noExemplaire']." (".$exemplaire['etat'].")"; ?></option>
                        <?php endforeach; ?>
                    </select>
                </label>
                <?php if (isset($erreurs['noExemplaire'])) echo '<div class="alert alter-danger">'.$erreurs['noExemplaire'].'</div>';?>
                <br>
                <br>
                <label>Date d'emprunt
                    <?php
                    if (isset($donnees['dateEmprunt'])) echo '<input name="newDateEmprunt" type="text" size="18" value="'.convert_date_us_fr($donnees['dateEmprunt']).'"/>';
                    else echo '<input name="newDateEmprunt" type="text" size="18" value=""/>';
                    ?>
                </label>
                <?php if (isset($erreurs['dateEmprunt'])) echo '<div class="alert alter-danger">'.$erreurs['dateEmprunt'].'</div>';?>
                <br>
                <br>
                <label>Date de restitution
                    <?php
                    if (isset($donnees['dateRendu'])) echo '<input name="newDateRendu" type="text" size="18" value="'.convert_date_us_fr($donnees['dateRendu']).'"/>';
                    else echo '<input name="newDateRendu" type="text" size="18" value=""/>';
                    ?>
                </label>
                <?php if (isset($erreurs['dateRendu'])) echo '<div class="alert alter-danger">'.$erreurs['dateRendu'].'</div>';?>
                <input type="submit" name="editEmprunt" value="Éditer l'emprunt"/>
            </fieldset>
        </div>
    </form>

<?php include("v_foot.php"); ?>