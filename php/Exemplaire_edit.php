<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET["noExemplaire"]) AND is_numeric($_GET["noExemplaire"]))
{
    $id=htmlentities($_GET['noExemplaire']);
    $ma_requete_SQL="SELECT ex.noExemplaire, ex.etat, ex.dateAchat, ex.prix, ex.noOeuvre FROM EXEMPLAIRE ex WHERE noExemplaire = ".$id.";";
    $reponse = $bdd->query($ma_requete_SQL);
    $donnees = $reponse->fetch();
}

if(isset($_POST['noExemplaire']) AND isset($_POST['etat']) AND isset($_POST['dateAchat']) AND isset($_POST['prix']) AND isset($_POST['noOeuvre']))
{
    $erreurs=array();
    $donnees['noExemplaire']=htmlentities($_POST['noExemplaire']);
    $donnees['etat']=htmlentities($_POST['etat']);
    $donnees['dateAchat']=htmlentities($_POST['dateAchat']);
    $donnees['prix']=htmlentities($_POST['prix']);
    $donnees['noOeuvre']=htmlentities($_POST['noOeuvre']);

    if (!is_numeric($donnees['prix'])) {
        $erreurs['prix']="Le prix doit être un nombre";
    }
    else if ($donnees['prix'] == "") {
        $erreurs['prix']="Le prix ne peut pas être vide";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateAchat'], $matches)) {
        $erreurs['dateAchat']='La date de parution doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['dateAchat']="La date n'est pas valide";
        } else {
            $donnees['dateAchat_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    if (empty($erreurs)) {
        $ma_requete_SQL="UPDATE EXEMPLAIRE SET etat='".$donnees['etat']."', dateAchat='".$donnees['dateAchat_us']."', prix=".$donnees['prix'].", noOeuvre=".$donnees['noOeuvre']." WHERE noExemplaire=".$donnees['noExemplaire'].";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Exemplaire_show.php?idOeuvre=".$donnees['noOeuvre']);
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }
}
?>

<form method="post" action="Exemplaire_edit.php">
    <div class="row">
        <fieldset>
            <legend>Modifier un exemplaire</legend>
            <input name="noOeuvre" type="hidden" value="<?php if(isset($donnees['noOeuvre'])) echo $donnees['noOeuvre']; ?>"/>
            <input name="noExemplaire" type="hidden" value="<?php if(isset($donnees['noExemplaire'])) echo $donnees['noExemplaire']; ?>"/>
            <label>Etat</label>
            <select name="etat">
                <option value="neuf"<?php if($donnees['etat'] == "neuf") echo " selected=\"selected\""; ?>>neuf</option>
                <option value="bon"<?php if($donnees['etat'] == "bon") echo " selected=\"selected\""; ?>>bon</option>
                <option value="moyen"<?php if($donnees['etat'] == "moyen") echo " selected=\"selected\""; ?>>moyen</option>
                <option value="mauvais"<?php if($donnees['etat'] == "mauvais") echo " selected=\"selected\""; ?>>mauvais</option>
            </select>
            <br>
            <br>
            <label>Date d'achat
                <?php
                if (isset($_POST['dateAchat'])) {
                    echo '<input name="dateAchat" type="text" size="18" value="'.$_POST['dateAchat'].'"/>';
                }
                else if (isset($donnees['dateAchat'])) {
                    echo '<input name="dateAchat" type="text" size="18" value="'.convert_date_us_fr($donnees['dateAchat']).'"/>';
                }
                else
                    echo '<input name="dateAchat" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['dateAchat'])) echo '<div class="alert alter-danger">'.$erreurs['dateAchat'].'</div>';?>
            <label>Prix
                <input name="prix" type="text" size="18" value="<?php if(isset($donnees['prix'])) echo $donnees['prix']; ?>"/>
            </label>
            <?php if (isset($erreurs['prix'])) echo '<div class="alert alter-danger">'.$erreurs['prix'].'</div>';?>
            <input type="submit" name="ModifierExemplaire" value="Modifier" />
        </fieldset>
    </div>
</form>
<?php include("v_foot.php"); ?>