<?php
include("connexion_bdd.php");

$id=htmlentities($_GET['id']);

$rq_sql ="SELECT COUNT(DISTINCT OEUVRE.noOeuvre) as totalOeuvre
            , COUNT(EXEMPLAIRE.noExemplaire) as totalExemplaire
            , COUNT(DISTINCT EMPRUNT.noExemplaire) as totalEmprunt
            , COUNT(DISTINCT e2.noExemplaire) as totalEmpruntEnCours
            FROM AUTEUR
            LEFT JOIN OEUVRE
            ON AUTEUR.idAuteur = OEUVRE.idAuteur
            LEFT JOIN EXEMPLAIRE
            ON OEUVRE.noOeuvre = EXEMPLAIRE.noOeuvre
            LEFT JOIN EXEMPLAIRE e2
            ON OEUVRE.noOeuvre = e2.noOeuvre
            AND e2.noExemplaire IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
            LEFT JOIN EMPRUNT
            ON EXEMPLAIRE.noExemplaire = EMPRUNT.noExemplaire
            WHERE AUTEUR.idAuteur=".$id.";";
$rp = $bdd->query($rq_sql);
$data = $rp->fetchAll();

foreach ($data as $key) {
    if ($key['totalOeuvre'] == 0 AND $key['totalExemplaire'] == 0 AND $key['totalEmprunt'] == 0 AND $key['totalEmpruntEnCours'] == 0) {
        $ma_requete_SQL="DELETE FROM AUTEUR WHERE idAuteur = ".$id.";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Auteur_show.php");
    }
}

print_r($data);
?>

<script>
    var r = confirm("<?php
        foreach ($data as $key) {
            echo "Supprimer cet auteur supprimera les "
                .$key['totalOeuvre']." oeuvres, "
                .$key['totalExemplaire']." exemplaires et "
                .$key['totalEmprunt']." emprunts dont ".$key['totalEmpruntEnCours']." en cours qui lui sont associés";
        }
        ?>");
    if (r) {
        r = false;
        document.location.href = "Auteur_delete.php?delete=true&<?php echo $_GET['id'] ?>";
    }
    else {
        document.location.href = "Auteur_delete.php?delete=false&<?php echo $_GET['id'] ?>";
    }
</script>