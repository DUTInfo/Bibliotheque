<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET["id"]) AND is_numeric($_GET["id"]))
{
    $id=htmlentities($_GET['id']);
    $ma_requete_SQL="SELECT ad.idAdherent, ad.nomAdherent, ad.adresse, ad.datePaiement FROM ADHERENT ad WHERE idAdherent = ".$id.";";
    $reponse = $bdd->query($ma_requete_SQL);
    $donnees = $reponse->fetch();
}

if(isset($_POST['nomAdherent']) AND isset($_POST['adresse']) AND isset($_POST['datePaiement']) AND isset($_POST['idAdherent']))
{
    $erreurs=array();
    $donnees['nomAdherent']=htmlentities($_POST['nomAdherent']);
    $donnees['adresse']=htmlentities($_POST['adresse']);
    $donnees['datePaiement']=htmlentities($_POST['datePaiement']);
    $donnees['idAdherent']=htmlentities($_POST['idAdherent']);

    if (!preg_match("/[A-Za-z]{2,}/", $donnees['nomAdherent'])) {
        $erreurs['nomAdherent'] = "Le nom de l'adhérent doit être composé d'au minimum deux lettres";
    }
    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['adresse'])) {
        $erreurs['adresse'] = "L'adresse doit être composé d'au minimum deux lettres et/ou chiffres";
    }
    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['datePaiement'], $matches)) {
        $erreurs['datePaiement']='La date de parution doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['datePaiement']="La date n'est pas valide";
        } else {
            $donnees['datePaiement_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    if (empty($erreurs)) {
        $ma_requete_SQL="UPDATE ADHERENT SET nomAdherent='".$donnees['nomAdherent']."'
        , adresse='".$donnees['adresse']."'
        , datePaiement='".$donnees['datePaiement_us']."' WHERE idAdherent=".$donnees['idAdherent'].";";
        var_dump($ma_requete_SQL);
        $bdd->exec($ma_requete_SQL);
        header("Location: Adherent_show.php");
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }

}
?>

<form method="post" action="Adherent_edit.php">
    <div class="row">
        <fieldset>
            <legend>Modifier un adhérent</legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <input name="idAdherent" type="hidden" value="<?php if(isset($donnees['idAdherent'])) echo $donnees['idAdherent']; ?>"/>
            <label>Nom de l'adhérent
                <input name="nomAdherent" type="text" size="18" value="<?php if(isset($donnees['nomAdherent'])) echo $donnees['nomAdherent']; ?>"/>
            </label>
            <?php if (isset($erreurs['nomAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['nomAdherent'].'</div>';?>
            <br>
            <label>Adresse
                <input name="adresse" type="text" size="18" value="<?php if(isset($donnees['adresse'])) echo $donnees['adresse']; ?>"/>
            </label>
            <?php if (isset($erreurs['adresse'])) echo '<div class="alert alter-danger">'.$erreurs['adresse'].'</div>';?>
            <br>
            <label>Date de paiement
                <?php
                    if (isset($_POST['datePaiement'])) {
                        echo '<input name="datePaiement" type="text" size="18" value="'.$_POST['datePaiement'].'"/>';
                    }
                    else if (isset($donnees['dateAchat'])) {
                        echo '<input name="datePaiement" type="text" size="18" value="'.convert_date_us_fr($donnees['datePaiement']).'"/>';
                    }
                    else
                        echo '<input name="datePaiement" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['datePaiement'])) echo '<div class="alert alter-danger">'.$erreurs['datePaiement'].'</div>';?>
            <input type="submit" name="ModifierOeuvre" value="Modifier" />
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>