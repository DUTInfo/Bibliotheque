<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if (isset($_GET['idOeuvre'])) {
    $ma_requete_SQL = "
    SELECT
    ex.etat
    , ex.noExemplaire
    , ex.dateAchat
    , ex.prix
    , ex.noOeuvre
    , oe.titre
    , au.nomAuteur
    , oe.dateParution
    FROM EXEMPLAIRE ex
    LEFT JOIN OEUVRE oe
    ON ex.noOeuvre = oe.noOeuvre
    LEFT JOIN AUTEUR au
    ON oe.idAuteur = au.idAuteur
    WHERE ex.noOeuvre = ".$_GET['idOeuvre']."
    ORDER BY oe.titre;
    ";

    $infos_SQL = "
    SELECT COUNT(E1.noExemplaire) AS NbExemplaire
    ,COUNT(E2.noExemplaire) as NbDispo
    FROM OEUVRE
    INNER JOIN AUTEUR
    ON AUTEUR.idAuteur = OEUVRE.idAuteur
    LEFT JOIN EXEMPLAIRE E1
    ON E1.noOeuvre = OEUVRE.noOeuvre
    LEFT JOIN EXEMPLAIRE E2
    ON E1.noExemplaire = E2.noExemplaire
    AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
    WHERE E1.noOeuvre = ".$_GET['idOeuvre']."
    GROUP BY OEUVRE.noOeuvre
    ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;
    ";
}
else {
    header("Location: Exemplaire_show.php");
}
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();

$reponse2 = $bdd->query($infos_SQL);
$infos = $reponse2->fetchAll();
?>

<div class="row">
    <a href="Exemplaire_add.php?idOeuvre=<?php echo $_GET['idOeuvre'] ?>">Ajouter un exemplaire</a>
    <?php foreach ($infos as $key): ?>
    <div class="alert">
        <strong>Disponibilité :</strong>
        Il y a
        <?php
            echo $key['NbExemplaire'];
        if (($key['NbExemplaire']) > 1) {
            echo " exemplaires";
        }
        else
            echo " exemplaire";
            ?> de cette Oeuvre dans la base, dont <?php
        echo $key['NbDispo'];
        if (($key['NbDispo']) > 1) {
            echo " exemplaires";
        }
        else
            echo " exemplaire";
        ?>
        encore disponibles.
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <?php endforeach; ?>
    <table border="2">
        <caption>Récapitulatifs des exemplaires</caption>
        <?php if(isset($donnees[0])): ?>
            <thead>
            <tr>
                <th>État</th>
                <th>Date d'achat</th>
                <th>Prix</th>
                <th>Nom de l'oeuvre</th>
                <th>Nom de l'auteur</th>
                <th>Date de parution</th>
                <th>Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($donnees as $value): ?>
                <tr>
                    <td>
                        <?php echo $value['etat']; ?>
                    </td>
                    <td>
                        <?php echo(convert_date_us_fr($value['dateAchat'])); ?>
                    </td>
                    <td>
                        <?php echo $value['prix']." €"; ?>
                    </td>
                    <td>
                        <?php echo $value['titre']; ?>
                    </td>
                    <td>
                        <?php echo $value['nomAuteur']; ?>
                    </td>
                    <td>
                        <?php echo convert_date_us_fr($value['dateParution']); ?>

                    </td>
                    <td>
                        <a class="lienTab" href="Exemplaire_edit.php?noExemplaire=<?= $value['noExemplaire']; ?>">Modifier</a>
                        <a class="lienTab" onclick='if(confirm("Êtes-vous sûr de vouloir supprimer cet exemplaire et les emprunts qui lui sont associés ?")) location.href="Exemplaire_delete.php?noExemplaire=<?= $value['noExemplaire']; ?>&idOeuvre=<?= $_GET[  'idOeuvre']; ?>";'>Supprimer</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tr>
                <td>Aucun exemplaire</td>
            </tr>
        <?php endif; ?>
    </table>
<div>
<?php include("v_foot.php"); ?>