<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

//traitement
if(isset($_POST['nomAdherent']) AND isset($_POST['adresse']) AND isset($_POST['datePaiement']))
{
    $erreurs=array();

    $donnees['nomAdherent']=htmlentities($_POST['nomAdherent']);
    $donnees['adresse']=htmlentities($_POST['adresse']);
    $donnees['datePaiement']=htmlentities($_POST['datePaiement']);

    if (!preg_match("/[A-Za-z]{2,}/", $donnees['nomAdherent'])) {
        $erreurs['nomAdherent'] = "Le nom de l'adhérent doit être composé d'au minimum deux lettres";
    }

    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['adresse'])) {
        $erreurs['adresse'] = "L'adresse doit être composé d'au minimum deux lettres et/ou chiffres";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['datePaiement'], $matches)) {
        $erreurs['datePaiement']='La date de parution doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['datePaiement']="La date n'est pas valide";
        } else {
            $donnees['datePaiement_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    if (empty($erreurs)) {
        $ma_requete_SQL="INSERT INTO ADHERENT (idAdherent,nomAdherent,adresse,datePaiement) VALUES (NULL,'".$donnees['nomAdherent']."','".$donnees['adresse']."','".$donnees['datePaiement_us']."');";
        $bdd->exec($ma_requete_SQL);
        header("Location: Adherent_show.php");
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }

}
$today = getdate();
//affichage de la vue
?>

<form method="post" action="Adherent_add.php">
    <div class="row">
        <fieldset>
            <legend>Ajouter un adhérent</legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <br>
            <label>Nom de l'adhérent
                <?php
                if (isset($donnees['nomAdherent'])) {
                    echo '<input name="nomAdherent" type="text" size="18" value="'.$donnees['nomAdherent'].'"/>';
                }
                else
                    echo '<input name="nomAdherent" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['nomAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['nomAdherent'].'</div>';?>
            <br>
            <label>Adresse de l'adhérent
                <?php
                if (isset($donnees['adresse'])) {
                    echo '<input name="adresse" type="text" size="18" value="'.$donnees['adresse'].'"/>';
                }
                else
                    echo '<input name="adresse" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['adresse'])) echo '<div class="alert alter-danger">'.$erreurs['adresse'].'</div>';?>
            <br>
            <label>Date de paiement
                <?php
                    if (isset($donnees['datePaiement'])) {
                        echo '<input name="datePaiement" type="text" size="18" value="'.$donnees['datePaiement'].'"/>';
                    }
                    else
                        echo '<input name="datePaiement" type="text" size="18" value="'.$today['mday'].'/'.$today['mon'].'/'.$today['year'].'"/>';
                ?>
            </label>
            <?php if (isset($erreurs['datePaiement'])) echo '<div class="alert alter-danger">'.$erreurs['datePaiement'].'</div>';?>
            <br>
            <input type="submit" name="addAdherent" value="Ajouter un adherent"/>
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>