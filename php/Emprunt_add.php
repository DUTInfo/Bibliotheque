<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET['idAdherent']))
{
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_GET['idAdherent']);
    $ma_requete_SQL="
        SELECT E2.noExemplaire
        , EXEMPLAIRE.noExemplaire AS noExemplaireExistant
        , E2.etat
        , OEUVRE.titre
        , OEUVRE.noOeuvre
        FROM EXEMPLAIRE
        INNER JOIN OEUVRE
        ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
        LEFT JOIN EXEMPLAIRE E2
        ON E2.noExemplaire = EXEMPLAIRE.noExemplaire
        AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
        WHERE E2.noExemplaire IS NOT NULL
        ORDER BY OEUVRE.titre ASC, E2.noExemplaire ASC";
    $reponse = $bdd->query($ma_requete_SQL);
    $donneesExemplaire = $reponse->fetchAll();
}

if(isset($_POST['idAdherent']) AND isset($_POST['noExemplaire']) AND isset($_POST['dateEmprunt']))
{
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_POST['idAdherent']);
    $donnees['noExemplaire']=htmlentities($_POST['noExemplaire']);
    $donnees['dateEmprunt']=htmlentities($_POST['dateEmprunt']);

    if (!preg_match("/[1-9]{1,}/", $donnees['idAdherent'])) {
        $erreurs['idAdherent'] = "L\'ID de l'adhérent est invalide";
    }

    if (!preg_match("/[1-9]{1,}/", $donnees['noExemplaire'])) {
        $erreurs['noExemplaire'] = "L'exemplaire selectionné ne peut pas être emprunté";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateEmprunt'], $matches))
        $erreurs['dateEmprunt']='La date d\'emprunt doit être au format JJ/MM/AAAA';

    else if (!checkdate($matches[2], $matches[1], $matches[3])) $erreurs['dateEmprunt']="La date n'est pas valide";

    else $donnees['dateEmprunt_us']=$matches[3]."-".$matches[2]."-".$matches[1];

    if (empty($erreurs)) {
        $ma_requete_SQL="INSERT INTO EMPRUNT (idAdherent,noExemplaire,dateEmprunt) VALUES ('".$donnees['idAdherent']."','".$donnees['noExemplaire']."','".$donnees['dateEmprunt_us']."');";
        $bdd->exec($ma_requete_SQL);
        header("Location: Emprunt_add.php?idAdherent=".$donnees['idAdherent']);
    }
    else $message = "Il y a des erreurs dans le formulaire :";
}

$ma_requete_SQL="SELECT idAdherent, nomAdherent FROM ADHERENT ORDER BY nomAdherent;";
$reponse = $bdd->query($ma_requete_SQL);
$donneesAdherent = $reponse->fetchAll();
$today = getdate();
?>

<?php if(!isset($donnees['idAdherent'])) : ?>
    <form method="get" action="Emprunt_add.php">
        <div class="row">
            <fieldset>
                <legend>Sélectionner un adhérent</legend>
                <?php if (!empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
                <label>Adherent :
                    <select name="idAdherent">
                        <?php if(!isset($donnees['idAdherent'])): ?>
                            <option value="" selected disabled>Choisir l'adherent</option>
                        <?php endif; ?>
                        <?php foreach ($donneesAdherent as $adherent) : ?>
                            <option value="<?php echo $adherent['idAdherent']; ?>"
                                <?php if(isset($donnees['idAdherent']) and $donnees['idAdherent'] == $adherent['idAdherent']) echo "selected"; ?>
                            ><?php echo $adherent['nomAdherent']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                </label>
                <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                <input type="submit" name="addEmprunt" value="Sélectionner cet adhérent"/>
            </fieldset>
        </div>
    </form>
<?php endif; ?>

<?php if(isset($donnees['idAdherent'])) : ?>
    <div class="row">
        <?php if (!empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
        <?php if (isset($erreurs['dateEmprunt'])) echo '<div class="alert alter-danger">'.$erreurs['dateEmprunt'].'</div>';?>
        <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
        <?php if (isset($erreurs['noExemplaire'])) echo '<div class="alert alter-danger">'.$erreurs['noExemplaire'].'</div>';?>
        <table border="2">
            <?php foreach ($donneesAdherent as $adherent) {
                if ($adherent['idAdherent'] == $donnees['idAdherent']) echo "<caption>Ajouter un emprunt à ".$adherent['nomAdherent']."</caption>";
            } ?>
            <?php if(isset($donneesExemplaire[0])): ?>
                <thead>
                <tr>
                    <th>Titre de l'oeuvre empruntée</th>
                    <th>Exemplaire</th>
                    <th>Opérations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($donneesExemplaire as $value): ?>
                    <tr>
                        <td>
                            <?php echo($value['titre']); ?>
                        </td>
                        <td>
                            <?php echo $value['noExemplaire']; ?>
                        </td>
                        <td>
                            <form method="post" action="Emprunt_add.php?idAdherent=<?= $donnees['idAdherent']; ?>">
                                <p>
                                    <input type="hidden" name="idAdherent" value="<?= $donnees['idAdherent']; ?>"/>
                                    <input type="hidden" name="noExemplaire" value="<?= $value['noExemplaire']; ?>"/>
                                    <input type="text" name="dateEmprunt" size="18" value="<?= $today['mday']; ?>/<?= $today['mon']; ?>/<?= $today['year']; ?>"/>
                                    <input type="submit" value="Emprunter" />
                                </p>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            <?php else: ?>
                <tr>
                    <td>Aucun exemplaire n'est disponible à l'emprunt.</td>
                </tr>
            <?php endif; ?>
        </table>
    <div>
<?php endif; ?>

<!--    <form method="post" action="Emprunt_add.php">-->
<!--        <div class="row">-->
<!--            <fieldset>-->
<!--                <legend>Ajouter une Emprunt</legend>-->
<!--                --><?php //if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
<!--                <label>Adherent :-->
<!--                    <select name="idAdherent">-->
<!--                        --><?php //if(!isset($donnees['idAdherent']) or $donnees['idAdherent'] == ""): ?>
<!--                            <option value="" selected disabled>Choisir l'adherent</option>-->
<!--                        --><?php //endif; ?>
<!--                        --><?php //foreach ($donneesAdherent as $adherent) : ?>
<!--                            <option value="--><?php //echo $adherent['idAdherent']; ?><!--"-->
<!--                                --><?php //if(isset($donnees['idAdherent']) and $donnees['idAdherent'] == $adherent['idAdherent']) echo "selected"; ?>
<!--                            >--><?php //echo $adherent['nomAdherent']; ?><!--</option>-->
<!--                        --><?php //endforeach; ?>
<!--                    </select>-->
<!--                    --><?php //if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
<!--                </label>-->
<!--                --><?php //if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
<!--                <br>-->
<!--                <br>-->
<!--                <label>Numéro d'exemplaire :-->
<!--                    <select name="noExemplaire">-->
<!--                        --><?php //if(!isset($donnees['noExemplaire']) or $donnees['noExemplaire'] == ""): ?>
<!--                            <option value="" selected disabled>Choisir le n° d'oeuvre</option>-->
<!--                        --><?php //endif; ?>
<!--                        --><?php //foreach ($donneesExemplaire as $exemplaire) : ?>
<!--                            <option value="--><?php //echo $exemplaire['noExemplaire']; ?><!--"-->
<!--                                --><?php //if(isset($donnees['noExemplaire']) and $donnees['noExemplaire'] == $exemplaire['noExemplaire']) echo "selected"; ?>
<!--                            >--><?php //echo $exemplaire['titre']." -- ".$exemplaire['noExemplaire']." (".$exemplaire['etat'].")"; ?><!--</option>-->
<!--                        --><?php //endforeach; ?>
<!--                    </select>-->
<!--                    --><?php //if (isset($erreurs['noExemplaire'])) echo '<div class="alert alter-danger">'.$erreurs['noExemplaire'].'</div>';?>
<!--                </label>-->
<!--                --><?php //if (isset($erreurs['noExemplaire'])) echo '<div class="alert alter-danger">'.$erreurs['noExemplaire'].'</div>';?>
<!--                <br>-->
<!--                <br>-->
<!--                <label>Date d'emprunt-->
<!--                    --><?php
//                    if (isset($donnees['dateEmprunt'])) echo '<input name="dateEmprunt" type="text" size="18" value="'.$donnees['dateEmprunt'].'"/>';
//                    else echo '<input name="dateEmprunt" type="text" size="18" value="'.$today['mday'].'/'.$today['mon'].'/'.$today['year'].'"/>';
//                    ?>
<!--                </label>-->
<!--                --><?php //if (isset($erreurs['dateEmprunt'])) echo '<div class="alert alter-danger">'.$erreurs['dateEmprunt'].'</div>';?>
<!--                <input type="submit" name="addEmprunt" value="Ajouter un emprunt"/>-->
<!--            </fieldset>-->
<!--        </div>-->
<!--    </form>-->

<?php include("v_foot.php"); ?>