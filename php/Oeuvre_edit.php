<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET["id"]) AND is_numeric($_GET["id"]))
{
    $id=htmlentities($_GET['id']);
    $ma_requete_SQL="SELECT oe.noOeuvre, oe.titre, oe.idAuteur, oe.dateParution FROM OEUVRE oe WHERE noOeuvre = ".$id.";";
    $reponse = $bdd->query($ma_requete_SQL);
    $donnees = $reponse->fetch();
}

if(isset($_POST['titre']) AND isset($_POST['dateParution']) AND isset($_POST['idAuteur']) AND isset($_POST['noOeuvre']))
{
    $erreurs=array();
    $donnees['titre']=htmlentities($_POST['titre']);
    $donnees['dateParution']=htmlentities($_POST['dateParution']);
    $donnees['idAuteur']=htmlentities($_POST['idAuteur']);
    $donnees['noOeuvre']=htmlentities($_POST['noOeuvre']);

    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['titre'])) {
        $erreurs['titre'] = "Le titre doit être composé d'au minimum deux lettres et/ou chiffres";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateParution'], $matches)) {
        $erreurs['dateParution']='La date de parution doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['dateParution']="La date n'est pas valide";
        } else {
            $donnees['dateParution_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    if (empty($erreurs)) {
        $ma_requete_SQL="UPDATE OEUVRE SET titre='".$donnees['titre']."'
        , dateParution='".$donnees['dateParution_us']."'
        , idAuteur=".$donnees['idAuteur']." WHERE noOeuvre=".$donnees['noOeuvre'].";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Oeuvre_show.php");
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }
}
$ma_requete_SQL="SELECT idAuteur, nomAuteur FROM AUTEUR ORDER BY nomAuteur;";
$reponse = $bdd->query($ma_requete_SQL);
$donneesAuteur = $reponse->fetchAll();
?>

<form method="post" action="Oeuvre_edit.php">
    <div class="row">
        <fieldset>
            <legend>Modifier une oeuvre</legend>
            <input name="noOeuvre" type="hidden" value="<?php if(isset($donnees['noOeuvre'])) echo $donnees['noOeuvre']; ?>"/>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <br>
            <label>Titre
                <input name="titre" type="text" size="18" value="<?php if(isset($donnees['titre'])) echo $donnees['titre']; ?>"/>
            </label>
            <?php if (isset($erreurs['titre'])) echo '<div class="alert alter-danger">'.$erreurs['titre'].'</div>';?>
            <label>Date de parution
                <?php
                    if (isset($_POST['dateParution'])) {
                        echo '<input name="dateParution" type="text" size="18" value="'.$_POST['dateParution'].'"/>';
                    }
                    else if (isset($donnees['dateParution'])) {
                        echo '<input name="dateParution" type="text" size="18" value="'.convert_date_us_fr($donnees['dateParution']).'"/>';
                    }
                    else
                        echo '<input name="dateParution" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['dateParution'])) echo '<div class="alert alter-danger">'.$erreurs['dateParution'].'</div>';?>
            <label>Auteur
                <select name="idAuteur">
                    <?php if(!isset($donnees['idAuteur']) or $donnees['idAuteur'] == ""): ?>
                        <option value="">Choisir l'auteur</option>
                    <?php endif; ?>
                    <?php foreach ($donneesAuteur as $auteur) : ?>
                        <option value="<?php echo $auteur['idAuteur']; ?>"
                            <?php if(isset($donnees['idAuteur']) and $donnees['idAuteur'] == $auteur['idAuteur']) echo "selected"; ?>
                        >
                            <?php echo $auteur['nomAuteur']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <?php if(isset($erreurs['idAuteur'])) echo '<small class="error">'.$erreurs['idAuteur'].'</small>'; ?>
            </label>
            <input type="submit" name="ModifierOeuvre" value="Modifier" />
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>