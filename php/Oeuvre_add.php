<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_POST['titre']) AND isset($_POST['dateParution']))
{
    $erreurs=array();
    $donnees['titre']=htmlentities($_POST['titre']);
    $donnees['dateParution']=htmlentities($_POST['dateParution']);

    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['titre'])) {
        $erreurs['titre'] = "Le titre doit être composé d'au minimum deux lettres et/ou chiffres";
    }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateParution'], $matches)) {
        $erreurs['dateParution']='La date de parution doit être au format JJ/MM/AAAA';
    }
    else {
        if (! checkdate($matches[2], $matches[1], $matches[3])) {
            $erreurs['dateParution']="La date n'est pas valide";
        } else {
            $donnees['dateParution_us']=$matches[3]."-".$matches[2]."-".$matches[1];
        }
    }

    //
    if (!isset($_POST['idAuteur'])) {
        $erreurs['idAuteur']="Choisissez un auteur";
        $message = "Il y a des erreurs dans le formulaire :";
    }
    else {
        $donnees['idAuteur']=htmlentities($_POST['idAuteur']);
        if (empty($erreurs)) {
            $ma_requete_SQL="INSERT INTO OEUVRE (noOeuvre,titre,dateParution,idAuteur) VALUES (NULL,'".$donnees['titre']."','".$donnees['dateParution_us']."',".$donnees['idAuteur'].");";
            $bdd->exec($ma_requete_SQL);
            header("Location: Oeuvre_show.php");
        }
        else {
            $message = "Il y a des erreurs dans le formulaire :";
        }
    }
    //
}
$ma_requete_SQL="SELECT idAuteur, nomAuteur FROM AUTEUR ORDER BY nomAuteur;";
$reponse = $bdd->query($ma_requete_SQL);
$donneesAuteur = $reponse->fetchAll();

?>

<form method="post" action="Oeuvre_add.php">
    <div class="row">
        <fieldset>
            <legend>Ajouter une Oeuvre</legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <label>Titre
                <?php
                if (isset($donnees['titre'])) {
                    echo '<input name="titre" type="text" size="18" value="'.$donnees['titre'].'"/>';
                }
                else
                    echo '<input name="titre" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['titre'])) echo '<div class="alert alter-danger">'.$erreurs['titre'].'</div>';?>
            <br>
            <label>Date de parution
                <?php
                    if (isset($donnees['dateParution'])) {
                        echo '<input name="dateParution" type="text" size="18" value="'.$donnees['dateParution'].'"/>';
                    }
                    else
                        echo '<input name="dateParution" type="text" size="18" value=""/>';
                ?>
            </label>
            <?php if (isset($erreurs['dateParution'])) echo '<div class="alert alter-danger">'.$erreurs['dateParution'].'</div>';?>
            <br>
            <label>Auteur
                <select name="idAuteur">
                    <?php if(!isset($donnees['idAuteur']) or $donnees['idAuteur'] == ""): ?>
                        <option value="" selected disabled>Choisir l'auteur</option>
                    <?php endif; ?>
                    <?php foreach ($donneesAuteur as $auteur) : ?>
                        <option value="<?php echo $auteur['idAuteur']; ?>"
                            <?php if(isset($donnees['idAuteur']) and $donnees['idAuteur'] == $auteur['idAuteur']) echo "selected"; ?>
                        >
                            <?php echo $auteur['nomAuteur']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <?php if (isset($erreurs['idAuteur'])) echo '<div class="alert alter-danger">'.$erreurs['idAuteur'].'</div>';?>
            </label>
            <input type="submit" name="addOeuvre" value="Ajouter une oeuvre"/>
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>