<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

// ## accès au modèle
$ma_requete_SQL = "
SELECT ADHERENT.idAdherent
, EXEMPLAIRE.noExemplaire
, OEUVRE.titre
, ADHERENT.nomAdherent
, EMPRUNT.dateEmprunt
, EMPRUNT.dateRendu
, DATEDIFF(CURRENT_DATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY)) AS NbJoursEmprunt
, DATEDIFF(CURRENT_DATE(), EMPRUNT.dateEmprunt) AS RETARD
, DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) AS DateRenduTheorique
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY), 1,0) AS flagRetard
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY), 1,0) AS flagPenalite
, IF( ((DATEDIFF(CURRENT_DATE(), DATE_ADD(dateEmprunt, INTERVAL 120 DAY)) *0.5) <25)
	,(DATEDIFF(CURRENT_DATE(), DATE_ADD(dateEmprunt, INTERVAL 120 DAY)) *0.5), 25) AS DETTE
FROM EMPRUNT
INNER JOIN ADHERENT
ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE
ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE
ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
WHERE EMPRUNT.dateRendu IS NULL
HAVING flagRetard = 1
ORDER BY EMPRUNT.dateEmprunt ASC;
";
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();
?>

<div class="row">
    <table border="2">
        <caption>Récapitulatif des emprunts non rendus</caption>
        <?php if (isset($donnees[0])): ?>
            <thead>
            <tr>
                <th>Nom de l'adhérent</th>
                <th>Titre</th>
                <th>Date d'emprunt</th>
                <th>Exemplaire</th>
                <th>Retard (jours)</th>
                <th>Pénalités</th>
            </tr>
            </thead>
            <tbody>
            <?php
                foreach ($donnees as $value):
                    if ($value['flagPenalite'] == 1) {
                        echo "<tr>";
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.$value['nomAdherent'].'</span></td>';
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.$value['titre'].'</span></td>';
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.convert_date_us_fr($value['dateEmprunt']).'</span></td>';
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.$value['noExemplaire'].'</span></td>';
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.$value['RETARD'].'</span></td>';
                            echo '<td><span style=\'color: #ff4f4f; background-color: #555555\'">'.$value['DETTE'].'</span></td>';
                        echo "</tr>";
                    }
                    else {
                        echo "<tr>";
                        echo '<td><span style=\'color: #ff804f; background-color: #555555\'">' . $value['nomAdherent'] . '</span></td>';
                        echo '<td><span style=\'color: #ff804f; background-color: #555555\'">' . $value['titre'] . '</span></td>';
                        echo '<td><span style=\'color: #ff804f; background-color: #555555\'">' . convert_date_us_fr($value['dateEmprunt']) . '</span></td>';
                        echo '<td><span style=\'color: #ff804f; background-color: #555555\'">' . $value['noExemplaire'] . '</span></td>';
                        echo '<td><span style=\'color: #ff804f; background-color: #555555\'">' . $value['RETARD'] . '</span></td>';
                        echo '<td></td>';
                        echo "</tr>";
                    }
                ?>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tr>
                <td>Aucun adhérent dans la base de données.</td>
            </tr>
        <?php endif; ?>
    </table>
    <div></div>
</div>
<?php include("v_foot.php"); ?>