<?php
include("connexion_bdd.php");
include("v_head.php");
include("v_nav.php");

if(isset($_GET["id"]) AND is_numeric($_GET["id"]))
{
    $id=htmlentities($_GET['id']);
    $ma_requete_SQL="SELECT au.idAuteur, au.nomAuteur, au.prenomAuteur FROM AUTEUR au WHERE idAuteur = ".$id.";";
    $reponse = $bdd->query($ma_requete_SQL);
    $donnees = $reponse->fetch();
}

if(isset($_POST['idAuteur']) AND isset($_POST['nomAuteur']) AND isset($_POST['prenomAuteur']))
{
    $erreurs=array();
    $donnees['idAuteur']=htmlentities($_POST['idAuteur']);
    $donnees['nomAuteur']=htmlentities($_POST['nomAuteur']);
    $donnees['prenomAuteur']=htmlentities($_POST['prenomAuteur']);

    if (!preg_match("/[A-Za-z1-9]{2,}/", $donnees['nomAuteur'])) {
        $erreurs['nomAuteur'] = "Le nom de l'auteur doit être composé d'au minimum deux lettres et/ou chiffres";
    }

    if (empty($erreurs)) {
        $ma_requete_SQL="UPDATE AUTEUR SET nomAuteur='".$donnees['nomAuteur']."'
        ,prenomAuteur='".$donnees['prenomAuteur']."'WHERE idAuteur=".$donnees['idAuteur'].";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Auteur_show.php");
    }
    else {
        $message = "Il y a des erreurs dans le formulaire :";
    }

}
?>

<form method="post" action="Auteur_edit.php">
    <div class="row">
        <fieldset>
            <legend>Modifier un auteur</legend>
            <?php if (! empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
            <input name="idAuteur" type="hidden" value="<?php if(isset($donnees['idAuteur'])) echo $donnees['idAuteur']; ?>"/>
            <label>Nom de l'auteur
                <input name="nomAuteur" type="text" size="18" value="<?php if(isset($donnees['nomAuteur'])) echo $donnees['nomAuteur']; ?>"/>
            </label>
            <?php if (isset($erreurs['nomAuteur'])) echo '<div class="alert alter-danger">'.$erreurs['nomAuteur'].'</div>';?>
            <label>Prénom de l'auteur
                <input name="prenomAuteur" type="text" size="18" value="<?php if(isset($donnees['prenomAuteur'])) echo $donnees['prenomAuteur']; ?>"/>
            </label>
            <input type="submit" name="ModifierAuteur" value="Modifier" />
        </fieldset>
    </div>
</form>

<?php include("v_foot.php"); ?>