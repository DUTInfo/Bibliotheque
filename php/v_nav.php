<nav>
    <div class="boutonMenu"><div class="titreMenu">Gestion des emprunts</div>
        <a class="SousMenu" href="Emprunt_add.php">Emprunter des livres</a>
        <a class="SousMenu" href="Emprunt_return.php">Rendre des livres</a>
        <a class="SousMenu" href="Emprunt_show.php">Afficher/Éditer/Supprimer des emprunts</a>
        <a class="SousMenu" href="Emprunt_bilan.php">Consulter les retards</a>
    </div>
    <div class="boutonMenu"><div class="titreMenu">Gestion des oeuvres et auteurs</div>
        <a class="SousMenu" href="Oeuvre_show.php">Afficher/Éditer/Supprimer des oeuvres</a>
        <a class="SousMenu" href="Auteur_show.php">Afficher/Éditer/Supprimer des auteurs</a>
    </div>
    <div class="boutonMenu"><div class="titreMenu">Gestion des adherents</div>
        <a class="SousMenu" href="Adherent_show.php">Afficher/Éditer/Supprimer des adhérents</a>
    </div>
</nav>