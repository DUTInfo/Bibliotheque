<?php
include("connexion_bdd.php");

$id=htmlentities($_GET['id']);

$rq_sql ="SELECT COUNT(*) as total
            FROM ADHERENT
            INNER JOIN EMPRUNT
            ON ADHERENT.idAdherent = EMPRUNT.idAdherent
            WHERE ADHERENT.idAdherent=".$id."
            AND EMPRUNT.dateRendu IS NULL;
        ";
$rp = $bdd->query($rq_sql);
$data = $rp->fetchAll();

foreach ($data as $key) {
    if ($key['total'] == 0) {
        $ma_requete_SQL="DELETE FROM ADHERENT WHERE idAdherent = ".$id.";";
        $bdd->exec($ma_requete_SQL);
        header("Location: Adherent_show.php");
    }
}

print_r($data);
?>

<script>
    var r = confirm("<?php
        foreach ($data as $key) {
            echo "Attention ! Supprimer cet adhérent supprimera ".$key['total']." emprunts associés.";
        }
        ?>");
    if (r == true) {
        document.location.href = "Adherent_delete.php?delete=true&<?php echo $_GET['id'] ?>";
    }
    else {
        document.location.href = "Adherent_show.php";
    }
</script>