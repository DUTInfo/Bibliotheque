<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

if(isset($_POST['idAdherent']) AND isset($_POST['noExemplaire']) AND isset($_POST['dateEmprunt']) AND isset($_POST['dateRendu'])) {
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_POST['idAdherent']);
    $donnees['noExemplaire']=htmlentities($_POST['noExemplaire']);
    $donnees['dateEmprunt']=htmlentities($_POST['dateEmprunt']);
    $donnees['dateRendu']=htmlentities($_POST['dateRendu']);

    if (!preg_match("/[1-9]{1,}/", $donnees['idAdherent'])) $erreurs['idAdherent'] = "L\'ID de l'adhérent est invalide";

    if (!preg_match("/[1-9]{1,}/", $donnees['noExemplaire'])) $erreurs['noExemplaire'] = "L'exemplaire selectionné ne peut pas être emprunté";

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateEmprunt'], $matches))
        $erreurs['dateEmprunt']='La date de restitution doit être au format JJ/MM/AAAA';

    else if (!checkdate($matches[2], $matches[1], $matches[3])) $erreurs['dateEmprunt']="La date entrée n'existe pas";

    else $donnees['dateEmprunt_us']=$matches[3]."-".$matches[2]."-".$matches[1];

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $donnees['dateRendu'], $matches))
        $erreurs['dateRendu']='La date d\'emprunt doit être au format JJ/MM/AAAA';

    else if (!checkdate($matches[2], $matches[1], $matches[3])) $erreurs['dateRendu']="La date entrée n'existe pas";

    else $donnees['dateRendu_us']=$matches[3]."-".$matches[2]."-".$matches[1];

    if (empty($erreurs)) {
        $ma_requete_SQL="UPDATE EMPRUNT SET dateRendu = '".$donnees['dateRendu_us']."'
        WHERE idAdherent = '".$donnees['idAdherent']."' AND noExemplaire = '".$donnees['noExemplaire']."'
        AND dateEmprunt = '".$donnees['dateEmprunt_us']."';";
        $bdd->exec($ma_requete_SQL);
        header("Emprunt_return.php?idAdherent=".$donnees['idAdherent']);
    }
    else $message = "Il y a des erreurs dans le formulaire :";
}

if(isset($_GET['idAdherent'])) {
    $erreurs=array();
    $donnees['idAdherent']=htmlentities($_GET['idAdherent']);
    $ma_requete_SQL = "
        SELECT EXEMPLAIRE.noExemplaire, OEUVRE.titre, EMPRUNT.dateEmprunt, ADHERENT.idAdherent
        FROM ADHERENT
        INNER JOIN EMPRUNT
        ON EMPRUNT.idAdherent = ADHERENT.idAdherent
        INNER JOIN EXEMPLAIRE
        ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
        INNER JOIN OEUVRE
        ON EXEMPLAIRE. noOeuvre = OEUVRE.noOeuvre
        WHERE EMPRUNT.dateRendu IS NULL AND EMPRUNT.idAdherent = '".$donnees['idAdherent']."'
        ORDER BY EMPRUNT.dateEmprunt DESC;";
    $reponse = $bdd->query($ma_requete_SQL);
    $donneesExemplaire = $reponse->fetchAll();
}

$ma_requete_SQL="SELECT idAdherent, nomAdherent FROM ADHERENT ORDER BY nomAdherent;";
$reponse = $bdd->query($ma_requete_SQL);
$donneesAdherent = $reponse->fetchAll();

$today = getdate(); ?>

<?php if(!isset($donnees['idAdherent'])) : ?>
    <form method="get" action="Emprunt_return.php">
        <div class="row">
            <fieldset>
                <legend>Sélectionner un adhérent</legend>
                <?php if (!empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
                <label>Adherent :
                    <select name="idAdherent">
                        <?php if(!isset($donnees['idAdherent'])): ?>
                            <option value="" selected disabled>Choisir l'adherent</option>
                        <?php endif; ?>
                        <?php foreach ($donneesAdherent as $adherent) : ?>
                            <option value="<?php echo $adherent['idAdherent']; ?>"
                                <?php if(isset($donnees['idAdherent']) and $donnees['idAdherent'] == $adherent['idAdherent']) echo "selected"; ?>
                            ><?php echo $adherent['nomAdherent']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                </label>
                <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
                <input type="submit" name="returnEmprunt" value="Sélectionner cet adhérent"/>
            </fieldset>
        </div>
    </form>
<?php endif; ?>

<?php if(isset($donnees['idAdherent'])) : ?>
    <div class="row">
        <?php if (!empty($erreurs)) echo '<div class="alert alter-danger">'.$message.'</div>';?>
        <?php if (isset($erreurs['dateRendu'])) echo '<div class="alert alter-danger">'.$erreurs['dateRendu'].'</div>';?>
        <?php if (isset($erreurs['idAdherent'])) echo '<div class="alert alter-danger">'.$erreurs['idAdherent'].'</div>';?>
        <?php if (isset($erreurs['noExemplaire'])) echo '<div class="alert alter-danger">'.$erreurs['noExemplaire'].'</div>';?>
        <table border="2">
            <?php foreach ($donneesAdherent as $adherent) {
                if ($adherent['idAdherent'] == $donnees['idAdherent']) echo "<caption>Récapitulatifs des emprunts de ".$adherent['nomAdherent']."</caption>";
            } ?>
            <?php if(isset($donneesExemplaire[0])): ?>
                <thead>
                <tr>
                    <th>Titre de l'oeuvre empruntée</th>
                    <th>Date d'emprunt</th>
                    <th>Exemplaire</th>
                    <th>Opérations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($donneesExemplaire as $value): ?>
                    <tr>
                        <td>
                            <?php echo($value['titre']); ?>
                        </td>
                        <td>
                            <?php echo convert_date_us_fr($value['dateEmprunt']); ?>
                        </td>
                        <td>
                            <?php echo $value['noExemplaire']; ?>
                        </td>
                        <td>
                            <form method="post" action="Emprunt_return.php?idAdherent=<?= $value['idAdherent']; ?>">
                                <p>
                                    <input type="hidden" name="idAdherent" value="<?= $value['idAdherent']; ?>"/>
                                    <input type="hidden" name="noExemplaire" value="<?= $value['noExemplaire']; ?>"/>
                                    <input type="hidden" name="dateEmprunt" value="<?= convert_date_us_fr($value['dateEmprunt']); ?>" />
                                    <input type="text" name="dateRendu" size="18" value="<?= $today['mday']; ?>/<?= $today['mon']; ?>/<?= $today['year']; ?>"/>
                                    <input type="submit" value="Rendre" />
                                </p>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            <?php else: ?>
                <tr>
                    <td>Aucun n'emprunt n'a été trouvé pour cet adhérent.</td>
                </tr>
            <?php endif; ?>
        </table>
    <div>
<?php endif; ?>

<?php include("v_foot.php"); ?>