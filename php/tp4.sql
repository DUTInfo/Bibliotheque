DROP TABLE IF EXISTS EMPRUNT;
DROP TABLE IF EXISTS EXEMPLAIRE;
DROP TABLE IF EXISTS OEUVRE;
DROP TABLE IF EXISTS ADHERENT;
DROP TABLE if EXISTS AUTEUR;

CREATE TABLE IF NOT EXISTS AUTEUR(
	idAuteur INT AUTO_INCREMENT,
	nomAuteur VARCHAR(25),
	prenomAuteur VARCHAR(25),
	PRIMARY KEY(idAuteur)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ADHERENT(
	idAdherent INT AUTO_INCREMENT,
	nomAdherent VARCHAR(25),
	adresse VARCHAR(25),
	datePaiement DATE,
	PRIMARY KEY (idAdherent)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS OEUVRE(
	noOeuvre INT AUTO_INCREMENT,
	titre VARCHAR(50),
	dateParution DATE,
	idAuteur INT,
	CONSTRAINT fk_oeuvre_auteur
		FOREIGN KEY (idAuteur) REFERENCES AUTEUR(idAuteur)
		ON DELETE CASCADE,
	PRIMARY KEY (noOeuvre)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS EXEMPLAIRE(
	noExemplaire INT AUTO_INCREMENT,
	etat ENUM("neuf", "bon", "moyen", "mauvais"),
	dateAchat DATE,
	prix DECIMAL,
	noOeuvre INT,
	CONSTRAINT fk_exemplaire_oeuvre
		FOREIGN KEY (noOeuvre) REFERENCES OEUVRE(noOeuvre)
		ON DELETE CASCADE,
	PRIMARY KEY (noExemplaire)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS EMPRUNT(
	idAdherent INT,
	noExemplaire INT,
	dateEmprunt DATE,
	dateRendu DATE,
	CONSTRAINT fk_emprunt_adherent
		FOREIGN KEY (idAdherent) REFERENCES ADHERENT(idAdherent)
		ON DELETE CASCADE,
	CONSTRAINT fk_emprunt_exemplaire
		FOREIGN KEY (noExemplaire) REFERENCES EXEMPLAIRE(noExemplaire)
		ON DELETE CASCADE,
	PRIMARY KEY(idAdherent, noExemplaire, dateEmprunt)
)ENGINE=InnoDB;

LOAD DATA LOCAL INFILE 'C:/UwAmp/www/Bibliotheque/csv/AUTEUR.csv' INTO TABLE AUTEUR character set utf8 FIELDS TERMINATED BY ",";
LOAD DATA LOCAL INFILE 'C:/UwAmp/www/Bibliotheque/csv/ADHERENT.csv' INTO TABLE ADHERENT character set utf8 FIELDS TERMINATED BY ",";
LOAD DATA LOCAL INFILE 'C:/UwAmp/www/Bibliotheque/csv/OEUVRE.csv' INTO TABLE OEUVRE character set utf8 FIELDS TERMINATED BY ",";
LOAD DATA LOCAL INFILE 'C:/UwAmp/www/Bibliotheque/csv/EXEMPLAIRE.csv' INTO TABLE EXEMPLAIRE character set utf8 FIELDS TERMINATED BY ",";
LOAD DATA LOCAL INFILE 'C:/UwAmp/www/Bibliotheque/csv/EMPRUNT.csv' INTO TABLE EMPRUNT character set utf8 FIELDS TERMINATED BY ",";

UPDATE EMPRUNT SET dateRendu = NULL
WHERE CAST(dateRendu AS CHAR(10))='0000-00-00';

UPDATE OEUVRE SET dateParution = NULL
WHERE CAST(dateParution AS CHAR(10)) = '0000-00-00';


-- Rq 1a
SELECT AUTEUR.nomAuteur, AUTEUR.prenomAuteur, AUTEUR.idAuteur, COUNT(OEUVRE.noOeuvre) AS NbrOeuvres
FROM AUTEUR
INNER JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
GROUP BY AUTEUR.idAuteur
ORDER BY AUTEUR.nomAuteur;

-- Rq 1b
SELECT AUTEUR.nomAuteur, AUTEUR.prenomAuteur, AUTEUR.idAuteur, COUNT(OEUVRE.noOeuvre) AS NbrOeuvres
FROM AUTEUR
LEFT JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
GROUP BY AUTEUR.idAuteur
ORDER BY AUTEUR.nomAuteur;

-- Rq 2a
SELECT AUTEUR.nomAuteur, OEUVRE.titre, OEUVRE.noOeuvre, COALESCE(OEUVRE.dateParution,'')  as dateParution, COUNT(EXEMPLAIRE.noExemplaire) AS NbExemplaire
FROM AUTEUR
INNER JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
INNER JOIN EXEMPLAIRE
ON OEUVRE.noOeuvre = EXEMPLAIRE.noOeuvre
GROUP BY OEUVRE.titre
ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;

-- Rq 2b
SELECT AUTEUR.nomAuteur, OEUVRE.titre, OEUVRE.noOeuvre, COALESCE(OEUVRE.dateParution,'')  as dateParution, COUNT(EXEMPLAIRE.noExemplaire) AS NbExemplaire
FROM AUTEUR
RIGHT JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
LEFT JOIN EXEMPLAIRE
ON OEUVRE.noOeuvre = EXEMPLAIRE.noOeuvre
GROUP BY OEUVRE.titre
ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;

-- Rq 2c
SELECT nomAuteur
FROM AUTEUR
LEFT JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur 
WHERE OEUVRE.noOeuvre IS NULL
GROUP BY AUTEUR.nomAuteur
ORDER BY AUTEUR.nomAuteur;

-- Rq 2d
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, OEUVRE.noOeuvre
, COALESCE(OEUVRE.dateParution,'')  as dateParution
, COUNT(E1.noExemplaire) AS NbExemplaire
,COUNT(E2.noExemplaire) as NbDispo
FROM OEUVRE
INNER JOIN AUTEUR
ON AUTEUR.idAuteur = OEUVRE.idAuteur
LEFT JOIN EXEMPLAIRE E1
ON E1.noOeuvre = OEUVRE.noOeuvre
LEFT JOIN EXEMPLAIRE E2
ON E1.noExemplaire = E2.noExemplaire
AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
GROUP BY OEUVRE.noOeuvre
ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;

-- Rq 3a
SELECT ADHERENT.nomAdherent
, ADHERENT.adresse
, ADHERENT.datePaiement
, ADHERENT.idAdherent
, COUNT(EMPRUNT.idAdherent) as NbEmprunt
, DATE_ADD(ADHERENT.datePaiement, INTERVAL 1 YEAR) AS DatePaiementFutur
FROM ADHERENT
LEFT JOIN EMPRUNT
ON EMPRUNT.idAdherent = ADHERENT.idAdherent
AND EMPRUNT.dateRendu IS NULL
GROUP BY ADHERENT.idAdherent
ORDER BY ADHERENT.nomAdherent;

-- Rq 3b
SELECT ADHERENT.nomAdherent
, ADHERENT.adresse
, ADHERENT.datePaiement
, ADHERENT.idAdherent
, COUNT(EMPRUNT.idAdherent) AS NbEmprunt
, IF(CURRENT_DATE()>DATE_ADD(ADHERENT.datePaiement, INTERVAL 1 YEAR), 1, 0) AS Retard
, IF(CURRENT_DATE()>DATE_ADD(ADHERENT.datePaiement, INTERVAL 11 MONTH), 1, 0) AS RetardProche
, DATE_ADD(ADHERENT.datePaiement, INTERVAL 1 YEAR) AS DatePaiementFutur
FROM ADHERENT
LEFT JOIN EMPRUNT
ON ADHERENT.idAdherent = EMPRUNT.idAdherent
AND EMPRUNT.dateRendu IS NULL
GROUP BY ADHERENT.idAdherent
ORDER BY ADHERENT.nomAdherent;

-- Rq 4a
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, COUNT(EXEMPLAIRE.noExemplaire) AS NbExemplaire
FROM OEUVRE
INNER JOIN AUTEUR
ON AUTEUR.idAuteur = OEUVRE.idAuteur
INNER JOIN EXEMPLAIRE
ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
GROUP BY AUTEUR.nomAuteur, OEUVRE.titre
ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;

SELECT COUNT(EXEMPLAIRE.noExemplaire) as NbrExempTotal
FROM EXEMPLAIRE;

-- Rq 4b

-- Rq 4c
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, OEUVRE.noOeuvre
, COUNT(E1.noExemplaire) AS NbExemplaire
, COUNT(E2.noExemplaire) AS nombreDispo
FROM OEUVRE
INNER JOIN AUTEUR
ON AUTEUR.idAuteur = OEUVRE.idAuteur
INNER JOIN EXEMPLAIRE E1
ON E1.noOeuvre = OEUVRE.noOeuvre
LEFT JOIN EXEMPLAIRE E2
ON E2.noExemplaire = E1.noExemplaire
AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
GROUP BY AUTEUR.nomAuteur, OEUVRE.titre, OEUVRE.noOeuvre, OEUVRE.dateParution
ORDER BY AUTEUR.nomAuteur ASC, OEUVRE.titre ASC;

-- RQ 5a
SELECT COUNT(EXEMPLAIRE.noExemplaire) AS nbrlocation
FROM EMPRUNT
INNER JOIN EXEMPLAIRE
ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
WHERE EMPRUNT.dateRendu IS NULL;

-- RQ 5b
SELECT COUNT(DISTINCT(ADHERENT.idAdherent)) AS nbrAdh_loc_non_rendu
FROM ADHERENT
INNER JOIN EMPRUNT
ON ADHERENT.idAdherent = EMPRUNT.idAdherent
WHERE EMPRUNT.dateRendu IS NULL;

-- RQ 5c
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, EXEMPLAIRE.noExemplaire
FROM AUTEUR
INNER JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
INNER JOIN EXEMPLAIRE
ON OEUVRE.noOeuvre = EXEMPLAIRE.noOeuvre
LEFT JOIN EMPRUNT
ON EXEMPLAIRE.noExemplaire = EMPRUNT.noExemplaire
WHERE EMPRUNT.dateEmprunt IS NULL;

-- RQ 6a
SELECT ADHERENT.nomAdherent
, EMPRUNT.dateEmprunt
, DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) AS date_rendu_max
FROM EMPRUNT
INNER JOIN ADHERENT
ON EMPRUNT.idAdherent = ADHERENT.idAdherent
WHERE EMPRUNT.dateRendu IS NULL
ORDER BY ADHERENT.idAdherent;

-- RQ 6b
SELECT ADHERENT.idAdherent
, EXEMPLAIRE.noExemplaire
, OEUVRE.titre
, ADHERENT.nomAdherent
, EMPRUNT.dateEmprunt
, DATEDIFF(curdate(), dateEmprunt) AS Retard
FROM ADHERENT
INNER JOIN EMPRUNT
ON ADHERENT.idAdherent = EMPRUNT.idAdherent
INNER JOIN EXEMPLAIRE
ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE
ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
WHERE EMPRUNT.dateRendu IS NULL
ORDER BY dateEmprunt DESC;

-- RQ 6c
SELECT ADHERENT.idAdherent
, EXEMPLAIRE.noExemplaire
, OEUVRE.titre
, ADHERENT.nomAdherent
, EMPRUNT.dateEmprunt, EMPRUNT.dateRendu
, DATEDIFF(CURRENT_DATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY)) AS NbJoursEmprunt
, DATEDIFF(CURRENT_DATE(), EMPRUNT.dateEmprunt) AS RETARD
, DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) AS DateRenduTheorique
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY), 1,0) AS flagRetard
, IF(CURRENT_DATE()>DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY), 1,0) AS flagPenalite
, IF( ((DATEDIFF(CURRENT_DATE(), DATE_ADD(dateEmprunt, INTERVAL 120 DAY)) *0.5) <25)
	,(DATEDIFF(CURRENT_DATE(), DATE_ADD(dateEmprunt, INTERVAL 120 DAY)) *0.5), 25) AS DETTE
FROM EMPRUNT
INNER JOIN ADHERENT
ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE
ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE
ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
WHERE EMPRUNT.dateRendu IS NULL
HAVING flagRetard = 1
ORDER BY EMPRUNT.dateEmprunt DESC;