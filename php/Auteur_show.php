<?php
include("connexion_bdd.php");
include("v_head.php");
include("v_nav.php");

$ma_requete_SQL = "
SELECT AUTEUR.nomAuteur
, AUTEUR.prenomAuteur
, AUTEUR.idAuteur
, COUNT(OEUVRE.noOeuvre) AS NbrOeuvres
FROM AUTEUR
LEFT JOIN OEUVRE
ON AUTEUR.idAuteur = OEUVRE.idAuteur
GROUP BY AUTEUR.idAuteur
ORDER BY AUTEUR.nomAuteur;";
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();
?>

<div class="row">
    <a href="Auteur_add.php">Ajouter un auteur</a>
    <div class="alert">
        <strong>Attention !</strong> Supprimer un auteur supprimera également ses oeuvres, les exemplaires de cette oeuvres ainsi que les emprunts en cours.
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <table border="2">
        <caption>Récapitulatifs des auteurs</caption>
        <?php if(isset($donnees[0])): ?>
            <thead>
                <tr>
                    <th>Nom de l'auteur</th>
                    <th>Prénom de l'auteur</th>
                    <th>Nombre d'oeuvres</th>
                    <th>Opérations</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($donnees as $value): ?>
                <tr>
                    <td>
                        <?php echo($value['nomAuteur']); ?>
                    </td>
                    <td>
                        <?php echo $value['prenomAuteur']; ?>
                    </td>
                    <td>
                        <?php echo $value['NbrOeuvres']; ?>
                    </td>
                    <td>
                        <a class="lienTab" href="Auteur_edit.php?id=<?= $value['idAuteur']; ?>">Modifier</a>
                        <a class='lienTab' href="Auteur_popup.php?id=<?= $value['idAuteur']; ?>">Supprimer</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tr>
                <td>Aucun auteur dans la base de données.</td>
            </tr>
        <?php endif; ?>
    </table>
</div>

<?php include("v_foot.php"); ?>