<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

// ## accès au modèle
$ma_requete_SQL = "
SELECT ADHERENT.nomAdherent
, ADHERENT.adresse
, ADHERENT.datePaiement
, ADHERENT.idAdherent
, COUNT(EMPRUNT.idAdherent) AS NbEmprunt
, IF(CURRENT_DATE()>DATE_ADD(ADHERENT.datePaiement, INTERVAL 1 YEAR), 1, 0) AS Retard
, IF(CURRENT_DATE()>DATE_ADD(ADHERENT.datePaiement, INTERVAL 11 MONTH), 1, 0) AS RetardProche
, DATE_ADD(ADHERENT.datePaiement, INTERVAL 1 YEAR) AS datePaiementFutur
FROM ADHERENT
LEFT JOIN EMPRUNT
ON ADHERENT.idAdherent = EMPRUNT.idAdherent
AND EMPRUNT.dateRendu IS NULL
GROUP BY ADHERENT.idAdherent
ORDER BY ADHERENT.nomAdherent;
";
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();
?>

<div class="row">
    <a href="Adherent_add.php">Ajouter un adhérent</a>
    <div class="alert">
        <strong>Attention !</strong> Supprimer un adhérent supprimera également l'historique de ses emprunts.
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
    <table border="2">
        <caption>Récapitulatifs des adhérents</caption>
        <?php if (isset($donnees[0])): ?>
            <thead>
            <tr>
                <th>Nom de l'adhérent</th>
                <th>Adresse</th>
                <th>Date de paiement</th>
                <th>Informations</th>
                <th>Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($donnees as $value): ?>
                <tr>
                    <td>
                        <?php echo($value['nomAdherent']); ?>
                    </td>
                    <td>
                        <?php echo $value['adresse']; ?>
                    </td>
                    <td>
                        <?php echo convert_date_us_fr($value['datePaiement']); ?>
                    </td>
                    <td>
                        <?php
                        if ($value['NbEmprunt'] > 1) {
                            echo $value['NbEmprunt']." emprunt(s) en cours";
                            echo "<br>";
                        }
                        if ($value['Retard'] == 1) {
                            echo "<span style='color: #ff4f4f; background-color: #555555'>"."Paiement en retard depuis : ".$value['datePaiementFutur']. "</span>";
                            echo "<br>";
                        }
                        if ($value['Retard'] == 0 and  $value['RetardProche'] == 1) {
                            echo "Paiement à renouveler";
                            echo "<br>";
                        }
                        ?>
                    </td>
                    <td>
                        <a class="lienTab" href="Adherent_edit.php?id=<?= $value['idAdherent']; ?>">Modifier</a>
                        <a class='lienTab' href="Adherent_popup.php?id=<?= $value['idAdherent']; ?>">Supprimer</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        <?php else: ?>
            <tr>
                <td>Aucun adhérent dans la base de données.</td>
            </tr>
        <?php endif; ?>
    </table>
    <div></div>
</div>
<?php include("v_foot.php"); ?>