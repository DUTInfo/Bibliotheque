<?php
include("connexion_bdd.php");
include("date_check.php");
include("v_head.php");
include("v_nav.php");

// ## accès au modèle
$ma_requete_SQL = "
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, OEUVRE.noOeuvre
, COALESCE(OEUVRE.dateParution,'')  as dateParution
, COUNT(E1.noExemplaire) AS NbExemplaire
,COUNT(E2.noExemplaire) as NbDispo
FROM OEUVRE
INNER JOIN AUTEUR
ON AUTEUR.idAuteur = OEUVRE.idAuteur
LEFT JOIN EXEMPLAIRE E1
ON E1.noOeuvre = OEUVRE.noOeuvre
LEFT JOIN EXEMPLAIRE E2
ON E1.noExemplaire = E2.noExemplaire
AND E2.noExemplaire NOT IN (SELECT EMPRUNT.noExemplaire FROM EMPRUNT WHERE EMPRUNT.dateRendu IS NULL)
GROUP BY OEUVRE.noOeuvre
ORDER BY AUTEUR.nomAuteur, OEUVRE.titre;
";
$reponse = $bdd->query($ma_requete_SQL);
$donnees = $reponse->fetchAll();
?>

<div class="row">
    <a href="Oeuvre_add.php">Ajouter une oeuvre</a>
    <div class="alert">
        <strong>Attention !</strong> Supprimer une oeuvre supprimera également les exemplaires de cette oeuvre, ainsi que les emprunts en cours.
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    </div>
	<table border="2">
		<caption>Récapitulatifs des oeuvres</caption>
        <?php if(isset($donnees[0])): ?>
			<thead>
				<tr>
                    <th>Nom de l'auteur</th>
                    <th>Titre de l'oeuvre</th>
                    <th>Date de parution</th>
                    <th>Nbr.</th>
                    <th>Nbr. Dispo.</th>
                    <th>Exemplaires</th>
                    <th>Opérations</th>
                </tr>
			</thead>
			<tbody>
				<?php foreach ($donnees as $value): ?>
				<tr>
					<td>
						<?php echo $value['nomAuteur']; ?>
					</td>
					<td>
						<?php echo($value['titre']); ?>
					</td>
					<td>
                        <?php echo convert_date_us_fr($value['dateParution']) ?>
					</td>
                    <td>
                        <?php echo $value['NbExemplaire']; ?>
                    </td>
                    <td>
                        <?php echo $value['NbDispo']; ?>
                    </td>
                    <td>
                        <a class="lienTab" href="Exemplaire_show.php?idOeuvre=<?= $value['noOeuvre']; ?>">Gérer les exemplaires</a>
                    </td>
                    <td>
                        <a class="lienTab" href="Oeuvre_edit.php?id=<?= $value['noOeuvre']; ?>">Modifier</a>
                        <a class="lienTab" href="Oeuvre_popup.php?id=<?= $value['noOeuvre']; ?>">Supprimer</a>
                    </td>
				</tr>
				<?php endforeach; ?>
			</tbody>
        <?php else: ?>
            <tr>
                <td>Pas d'oeuvre dans la base de données.</td>
            </tr>
        <?php endif; ?>
	</table>
<div>

<?php include("v_foot.php"); ?>